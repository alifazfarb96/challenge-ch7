const { history, room, user } = require('../models')
const generatePassword = require('generate-password')

module.exports = {

    createRoom: async (req, res) => {
        try {
            const userData = await user.findOne({ where : {
                id: res.user.id
            }
            })
            
            const id = userData.id;

            const roomData = await room.create({
                room_name: req.body.room_name,
                room_code: generatePassword.generate({
                    length: 6,
                    uppercase: false
                }),
                date_created: Date.now(),
                is_active: true,
                player1_id: id,
                player2_id: null,
                winner: null
            });

            const historyDataPlayer1 = await history.create({
                user_id: id,
                game_id: roomData.id,
                
            });

            return res.json({
                message: "Berhasil membuat room",
                data: { roomData, historyDataPlayer1 }
            })
        } catch (error) {
            console.log(error)
            return res.json({
                message: "Fatal Error!"
            })
    }
    },

    listRoom: async (req, res) => {
        try {
            const data = await room.findAll(
                {}
            );

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },

    joinRoom: async (req, res) => {
        try {
            const roomData = await room.findOne({ where : {
                room_name: req.body.room_name,
                room_code: req.body.room_code
            }
            })

            console.log(roomData)

            if (!roomData) {
                return res.json({
                    error: "nama atau kode room salah",
                })
            }

            if (roomData.player2_id != null && roomData.is_active === true) {
                return res.json({
                    error: "Room telah terisi!",
                })
            }

            if (roomData.is_active === false) {
                return res.json({
                    error: "Room kadaluarsa",
                })
            }

            const userData = await user.findOne({ where : {
                id: res.user.id
            }
            })
            
            const id = userData.id;

            
            if (roomData.player1_id === id) {
                return res.json({
                    error: "Room tidak bisa diisi oleh orang yang sama!",
                })
            }
            
            const data = await room.update({
                player2_id: id
            }, {
                where : {
                    room_name: req.body.room_name,
                    room_code: req.body.room_code,
                    is_active: true
                }
            })

            const historyDataPlayer2 = await history.create({
                user_id: id,
                game_id: roomData.id,
                
            });

            return res.json({
                message: "Berhasil masuk room. Memulai pertandingan",
                data: { data, historyDataPlayer2 }
            })

        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },

    fight: async(req,res) => {
        try {
            const choiceList = ['R', 'P', 'S']

            const room_name = req.params.room_name
            const room_code = req.params.room_code

            const userId = res.user.id

            const roomData = await room.findOne({ where : {
                room_name: room_name,
                room_code: room_code
            }
            })

            if (!room_name && !room_code) {
                return res.json({
                    message : "Room tidak valid!"
                })
            }

            if (!roomData.is_active) {
                return res.json({
                    message : "Room telah berakhir"
                })
            }

            const roomUserHistory = await history.findOne({ where : {
                user_id: userId,
                game_id: roomData.id
            }
            })

            // tentukan id player 1 dan id player 2
            let oppId = undefined
            if (roomData.player1_id === userId) {
                oppId =  roomData.player2_id
            } else{ oppId = roomData.player1_id}
        
            // baca history lawan untuk game yang sama
            const roomOppHistory = await history.findOne({ where : {
                user_id: oppId,
                game_id: roomData.id
            }
            })

            // Mencatat input
            const inputChoice = req.body.choice.toUpperCase()
            const inputChoiceId = choiceList.indexOf(inputChoice)

            if (inputChoiceId === -1) {
                return res.json({
                    error: "Pilihan tidak valid!"
                })
            }


            // Menyiapkan letak input pada history
            var msg = undefined
            if (roomUserHistory.choice_round_1 === null) {
                const data = await history.update({
                    choice_round_1: inputChoice
                }, {
                    where : {
                        user_id: userId,
                        game_id: roomData.id
                    }
                })
                const oppChoice = roomOppHistory.choice_round_1

                if (oppChoice === null) {
                    msg = `Ronde 1 - Anda memilih ${inputChoice}. Menunggu respon lawan`
                    return res.json({
                        message: msg,
                        data: data
                    })
                } else if (oppChoice != null) {
                    const oppChoiceId = choiceList.indexOf(oppChoice)
                    
                    const resultUser = getResult(inputChoiceId, oppChoiceId)
                    const userFinal = await history.update({
                        result_round_1: resultUser.text,
                        score: resultUser.score,
                    }, {
                        where : {
                            user_id: userId,
                            game_id: roomData.id
                        }
                    })

                    const resultOpp = getResult(oppChoiceId, inputChoiceId)
                    const oppFinal = await history.update({
                        result_round_1: resultOpp.text,
                        score: resultOpp.score,
                    }, {
                        where : {
                            user_id: oppId,
                            game_id: roomData.id
                        }
                    })
                    msg = `Ronde 1 Selesai. Hasil: ${resultUser.text}`
                    return res.json({
                        message: msg,
                        data: { userFinal, oppFinal}
                    })
                }
                
            } else if(roomUserHistory.choice_round_2 === null){
                const data = await history.update({
                    choice_round_2: inputChoice
                }, {
                    where : {
                        user_id: userId,
                        game_id: roomData.id
                    }
                })
                const oppChoice = roomOppHistory.choice_round_2

                if (oppChoice === null) {
                    msg = `Ronde 2 - Anda memilih ${inputChoice}. Menunggu respon lawan`
                    return res.json({
                        message: msg,
                        data: data
                    })
                } else if (oppChoice != null) {
                    const oppChoiceId = choiceList.indexOf(oppChoice)
                    
                    const resultUser = getResult(inputChoiceId, oppChoiceId)
                    const userFinal = await history.update({
                        result_round_2: resultUser.text,
                        score: resultUser.score + roomUserHistory.score,
                    }, {
                        where : {
                            user_id: userId,
                            game_id: roomData.id
                        }
                    })

                    const resultOpp = getResult(oppChoiceId, inputChoiceId)
                    const oppFinal = await history.update({
                        result_round_2: resultOpp.text,
                        score: resultOpp.score + roomOppHistory.score,
                    }, {
                        where : {
                            user_id: oppId,
                            game_id: roomData.id
                        }
                    })
                    msg = `Ronde 2 Selesai. Hasil: ${resultUser.text}`
                    return res.json({
                        message: msg,
                        data: { userFinal, oppFinal}
                    })
                }
            } else if(roomUserHistory.choice_round_3 === null){
                const data = await history.update({
                    choice_round_3: inputChoice
                }, {
                    where : {
                        user_id: userId,
                        game_id: roomData.id
                    }
                })
                const oppChoice = roomOppHistory.choice_round_3

                if (oppChoice === null) {
                    msg = `Ronde 3 - Anda memilih ${inputChoice}. Menunggu respon lawan`
                    return res.json({
                        message: msg,
                        data: data
                    })
                } else if (oppChoice != null) {
                    const oppChoiceId = choiceList.indexOf(oppChoice)
                    
                    const resultUser = getResult(inputChoiceId, oppChoiceId)
                    const userFinalScore = resultUser.score + roomUserHistory.score
                    const userFinal = await history.update({
                        result_round_3: resultUser.text,
                        score: userFinalScore
                    }, {
                        where : {
                            user_id: userId,
                            game_id: roomData.id
                        }
                    })

                    const resultOpp = getResult(oppChoiceId, inputChoiceId)
                    const oppFinalScore = resultOpp.score + roomOppHistory.score
                    const oppFinal = await history.update({
                        result_round_3: resultOpp.text,
                        score: oppFinalScore,
                    }, {
                        where : {
                            user_id: oppId,
                            game_id: roomData.id
                        }
                    })

                    console.log(userFinalScore, oppFinalScore)
                    var winner_id = undefined
                    var winner_score = undefined

                    if (userFinalScore > oppFinalScore) {

                        var userFinalResultText = "Win"
                        var oppFinalResultText = "Lose"
                        var winner_id = userId
                        var winner_score = userFinalScore
                    } else if (userFinalScore < oppFinalScore) { 
                        var userFinalResultText = "Lose"
                        var oppFinalResultText = "Win"
                        var winner_id = oppId
                        var winner_score = oppFinalScore
                    } else {
                        var userFinalResultText = "Draw"
                        var oppFinalResultText = "Draw"
                        var winner_id = 0
                        var winner_score = userFinalScore
                    }
                    
                    const userFinalResult = await history.update({
                        final_result: userFinalResultText
                    }, {
                        where : {
                            user_id: userId,
                            game_id: roomData.id
                        }
                    })
                    const oppFinalResult = await history.update({
                        final_result: oppFinalResultText
                    }, {
                        where : {
                            user_id: oppId,
                            game_id: roomData.id
                        }
                    })
                    
                    msg = `Ronde 3 Selesai. Pemenang: user-id-${winner_id}. Skor: ${winner_score}`

                    const roomFinal = await room.update({
                        is_active: false,
                        winner: winner_id
                    }, {
                        where : {
                            room_name: room_name,
                            room_code: room_code
                        }
                        
                    })


                    return res.json({
                        message: msg,
                        data: { userFinal, oppFinal, roomFinal}
                    })
                }
            }
            
            return res.json({
                message: "OK",
            })



        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    }
}

function getResult(userID, oppID) {
    if (userID === oppID) {
        return {
            text: "Seri",
            score: 1
        }

    } else if (userID > oppID) {
        if (userID === 2 && oppID === 0) {
            return {
                text: "Kalah",
                score: 0
            }
        } else {
            return {
                text: "Menang",
                score: 3
            }
        }

    } else if (userID < oppID) {
        if (userID === 0 && oppID === 2) {
            return {
                text: "Menang",
                score: 3
            }
        } else {
            return {
                text: "Kalah",
                score: 0
            }
        }
    }
}