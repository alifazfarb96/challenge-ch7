const {admin, user, profile, room, history } = require('../models')

const bcrypt = require('bcrypt')
const { body, validationResult } = require('express-validator')
const jwt = require('jsonwebtoken')

module.exports = {
    register: async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array(), message: "Periksa kembali data anda!" });
            }

            const password = bcrypt.hashSync(req.body.password, 10)
            const adminData = await admin.create({
                email: req.body.email,
                username: req.body.username,
                password: password,
            });

            return res.json({
                message: "Register berhasil. Silahkan melakukan login",
                data: adminData
            })
        } catch (error) {
            console.log(error)
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    
    login: async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            const adminData = await admin.findOne({ where : {
                email: req.body.email
            }})

            if(!adminData) {
                return res.json({
                    message : "User tidak ditemukan"
                })
            }

            if (bcrypt.compareSync(req.body.password, adminData.password)) {
                const token = jwt.sign(
                  { id: adminData.id },
                  'admin@RPS',
                  { expiresIn: '6h' }
                )
          
                return res.status(200).json({
                    token: token
                })
              }
            
            return res.json({
                message : "Password salah"
            })

        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },

    listUser: async (req, res) => {
        try {
            const data = await user.findAll(
                {
                    include: [{ 
                        model: profile
                    }]
                }
            );

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    
    createUser: async (req, res) => {
        try {
            const password = bcrypt.hashSync(req.body.password, 10)
            const data = await user.create({
                username: req.body.username,
                email: req.body.email,
                password: password,
            });

            const profileDefault = await profile.create({
                user_id: data.id,
                first_name: null,
                last_name: null,
                dob: null,
                gender: null,
            });

            return res.json({
                data: { data, profileDefault }
            })
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },

    editUserProfile: async (req,res) => {
        try {
            const data = await profile.update({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                dob: req.body.dob,
                gender: req.body.gender,
            }, {
                where : {
                    user_id: req.body.user_id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroyUserProfile: async (req, res) => {
        try {
            const data = await profile.update({
                first_name: null,
                last_name: null,
                dob: null,
                gender: null,
            },
                {
                where : {
                    user_id: req.body.user_id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },

    listHistory: async (req, res) => {
        try {
            const data = await history.findAll(
                {}
            );

            return res.json({
                data: data
            })
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
} 
