const { user, profile } = require('../models')

const bcrypt = require('bcrypt')
const { body, validationResult } = require('express-validator')
const jwt = require('jsonwebtoken')

module.exports = {
    register: async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array(), message: "Periksa kembali data anda!" });
            }

            const password = bcrypt.hashSync(req.body.password, 10)
            const userData = await user.create({
                email: req.body.email,
                username: req.body.username,
                password: password,
            });

            const profileDefault = await profile.create({
                user_id: userData.id,
                first_name: null,
                last_name: null,
                dob: null,
                gender: null,
            });

            return res.json({
                message: "Register berhasil. Silahkan melakukan login",
                data: { userData, profileDefault }
            })
        } catch (error) {
            console.log(error)
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    
    login: async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            const userData = await user.findOne({ where : {
                email: req.body.email
            }})

            if(!userData) {
                return res.json({
                    message : "User tidak ditemukan"
                })
            }

            if (bcrypt.compareSync(req.body.password, userData.password)) {
                const token = jwt.sign(
                  { id: userData.id },
                  'user@RPS',
                  { expiresIn: '6h' }
                )
          
                return res.status(200).json({
                    token: token
                })
              }
            
            return res.json({
                message : "Password salah"
            })

        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },

    loadProfile: async(req, res) => {
        try {
            const userData = await user.findOne({ where : {
                id: res.user.id
            }
            })
            
            const id = userData.id;
            
            const userProfile = await profile.findOne({ where : {
                user_id: id
            }})
            
            return res.json({
                data : userProfile
            })
            
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },

    editProfile: async (req,res) => {
        try {
            const userData = await user.findOne({ where : {
                id: res.user.id
            }
            })
            
            const id = userData.id;
            
            const data = await profile.update({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                dob: req.body.dob,
                gender: req.body.gender,
            }, {
                where : {
                    user_id: id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },

} 
