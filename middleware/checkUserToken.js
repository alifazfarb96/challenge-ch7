const jwt = require('jsonwebtoken');
require('dotenv').config();

const checkToken = (req, res, next) => {
    let token = req.headers.authorization
  
    if (!token) {
      return res.status(403).json({
        message: 'Token tidak ditemukan'
      })
    }
  
    // if provided with Bearer then remove it
    if (token.toLowerCase().startsWith('bearer')) {
      token = token.slice('bearer'.length).trim()
    }
  
    try {
      const jwtPayload = jwt.verify(token, 'user@RPS')
  
      if (!jwtPayload) { return res.status(403).json({ message: 'Tidak terautentifikasi. Silahkan melakukan login ulang'}) }
  
      res.user = jwtPayload
  
      next()
    } catch (error) {
      return res.status(403).json({ message: 'Autentifikasi gagal. Silahkan melakukan login ulang'})
    }
  }
  
module.exports = checkToken
