'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.user, {
        foreignKey: 'player1_id'
      }),
      this.belongsTo(models.user, {
        foreignKey: 'player2_id'
      })
    }
  }
  room.init({
    room_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    room_code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    date_created: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    player1_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    player2_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    winner: {
      type: DataTypes.STRING,
      allowNull: true,
    }
  }, {
    sequelize,
    modelName: 'room',
    tableName: 'rooms'
  });
  return room;
};