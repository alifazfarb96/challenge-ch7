'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.profile, {
        foreignKey: 'user_id'
      }),
      this.hasMany(models.room, {
        foreignKey: 'player1_id'
      }),
      this.hasMany(models.room, {
        foreignKey: 'player2_id'
      }),
      this.hasMany(models.history, {
        foreignKey: 'user_id'
      })
      
    }
  }
  user.init({
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
  }, {
    sequelize,
    modelName: 'user',
    tableName: 'users'
  });
  return user;
};