'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.user, {
        foreignKey: 'user_id'
      }),
      this.belongsTo(models.room, {
        foreignKey: 'game_id'
      })
    }
  }
  history.init({
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    game_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    choice_round_1: {
      type: DataTypes.STRING,
      allowNull: true
    },
    result_round_1: {
      type: DataTypes.STRING,
      allowNull: true
    },
    choice_round_2: {
      type: DataTypes.STRING,
      allowNull: true
    },
    result_round_2: {
      type: DataTypes.STRING,
      allowNull: true
    },
    choice_round_3: {
      type: DataTypes.STRING,
      allowNull: true
    },
    result_round_3: {
      type: DataTypes.STRING,
      allowNull: true
    },
    final_result: {
      type: DataTypes.STRING,
      allowNull: true
    },
    score: {
      type: DataTypes.STRING,
      allowNull: true
    },
  }, {
    sequelize,
    modelName: 'history',
    tableName: 'histories'
  });
  return history;
};