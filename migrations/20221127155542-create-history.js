'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('histories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER
      },
      game_id: {
        type: Sequelize.INTEGER
      },
      choice_round_1: {
        type: Sequelize.STRING
      },
      result_round_1: {
        type: Sequelize.STRING
      },
      choice_round_2: {
        type: Sequelize.STRING
      },
      result_round_2: {
        type: Sequelize.STRING
      },
      choice_round_3: {
        type: Sequelize.STRING
      },
      result_round_3: {
        type: Sequelize.STRING
      },
      final_result: {
        type: Sequelize.STRING
      },
      score: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('histories');
  }
};