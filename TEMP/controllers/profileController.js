const {user, profile } = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const data = await profile.findAll(
                {
                include: [
                    {model: user}
                ]
            }
            );

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await profile.create({
                user_id: req.body.user_id,
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                dob: req.body.dob,
                gender: req.body.gender,
            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    update: async (req,res) => {
        try {
            const data = await profile.update({
                user_id: req.body.user_id,
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                dob: req.body.dob,
                gender: req.body.gender,
            }, {
                where : {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await profile.destroy({
                where : {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    }
} 
