const { user, history, profile } = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const data = await user.findAll(
                {
                include: [
                        { model: history },
                        {model: profile},
                ]
            }
            );

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    register: async (req, res) => {
        try {
            const data = await user.create({
                email: req.body.email,
                username: req.body.username,
                password: req.body.password
            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    update: async (req,res) => {
        try {
            const data = await user.update({
                email: req.body.email,
                username: req.body.username,
                password: req.body.password
            }, {
                where : {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await user.destroy({
                where : {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    }
} 
