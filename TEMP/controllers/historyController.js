const { user, history } = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const data = await history.findAll(
            {
                include: [
                    {model: user}
                ]
            }
            );
            return res.json({
                data: data
            })
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await history.create({
                user_id: req.body.user_id,
                time_access: req.body.time_access,
                user_choice: req.body.user_choice,
                com_choice: req.body.com_choice,
                result: req.body.result,
            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    update: async (req,res) => {
        try {
            const data = await history.update({
                user_id: req.body.user_id,
                time_access: req.body.time_access,
                user_choice: req.body.user_choice,
                com_choice: req.body.com_choice,
                result: req.body.result,
            }, {
                where : {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await history.destroy({
                where : {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    }
} 
