'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_histories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.user_games, {
        foreignKey: 'user_id'//'user_game_histories_user_id_fkey'
      })
    }
  }
  user_game_histories.init({
    user_id: DataTypes.INTEGER,
    time_access: DataTypes.DATE,
    user_choice: DataTypes.STRING,
    com_choice: DataTypes.STRING,
    result: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_game_histories',
  });
  return user_game_histories;
};