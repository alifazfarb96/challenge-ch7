'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_games extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.user_game_bios, {
        foreignKey: 'user_id' //'user_game_bios_user_id_fkey'
      }),
      this.hasMany(models.user_game_histories, {
        foreignKey: 'user_id' //user_game_histories_user_id_fkey'
      })

    }
  }
  user_games.init({
    email: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_games',
  });
  return user_games;
};