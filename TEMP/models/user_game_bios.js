'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_bios extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.user_games, {
        foreignKey: 'user_id' //'user_game_bios_user_id_fkey'
      })
    }
  }
  user_game_bios.init({
    user_id: DataTypes.INTEGER,
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    dob: DataTypes.DATEONLY,
    gender: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_game_bios',
  });
  return user_game_bios;
};