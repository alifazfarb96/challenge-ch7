const express = require('express')
const router = express.Router()
const userController = require('./controllers/userController');
const adminController = require('./controllers/adminController');
const gameController = require('./controllers/gameController');
const pageController = require('./controllers/pageController');
const { body } = require('express-validator');
const checkUserToken = require('./middleware/checkUserToken');
const checkAdminToken = require('./middleware/checkAdminToken');

// page controller
router.get('/', pageController.home);
router.get('/game', pageController.game);

// untuk User
router.post('/api/user/register', body('email').isEmail(), body('username').notEmpty(), body('password').notEmpty(), userController.register)
router.post('/api/user/login', body('email').isEmail().notEmpty(), body('password').notEmpty(), userController.login);
router.get('/api/user/profile', checkUserToken, userController.loadProfile)
router.put('/api/user/profile/edit', checkUserToken, userController.editProfile)

// untuk SuperAdmin
router.post('/api/admin/register', body('email').isEmail(), body('username').notEmpty(), body('password').notEmpty(), adminController.register)
router.post('/api/admin/login', body('email').isEmail().notEmpty(), body('password').notEmpty(), adminController.login);
router.get('/api/admin/user/list', checkAdminToken, adminController.listUser)
router.post('/api/admin/user/create', checkAdminToken, adminController.createUser)
router.put('/api/admin/user/profile/edit', checkAdminToken, adminController.editUserProfile)
router.delete('/api/admin/user/profile/delete', checkAdminToken, adminController.destroyUserProfile)
router.get('/api/admin/history/list', checkAdminToken, adminController.listHistory)

// untuk Gameplay
router.get('/api/room/list', checkUserToken, gameController.listRoom)
router.post('/api/room/create', checkUserToken, gameController.createRoom)
router.post('/api/room/join', checkUserToken, gameController.joinRoom)
router.post('/api/fight/:room_name/:room_code', checkUserToken, gameController.fight)





module.exports = router
